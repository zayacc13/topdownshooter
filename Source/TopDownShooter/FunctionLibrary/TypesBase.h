// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "TypesBase.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	AimState UMETA(DisplayName = "AimState"),
	AimWalkState UMETA(DisplayName = "AimWalkState"),
	WalkState UMETA(DisplayName = "WalkState"),
	RunState UMETA(DisplayName = "RunState"),
	SprintRunState UMETA(DisplayName = "SprintRunState")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeedNormal = 600.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnyWhere, BlueprintReadWrite, Category = "Movement")
		float SprintRunSpeed = 800.0f;
};	

/*
UCLASS()
class TopDownShooter_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

};
*/