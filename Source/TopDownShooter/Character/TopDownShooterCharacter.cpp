// Copyright Epic Games, Inc. All Rights Reserved.

#include "TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

ATopDownShooterCharacter::ATopDownShooterCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATopDownShooterCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	MovementTick(DeltaSeconds);	
}

void ATopDownShooterCharacter::SetupPlayerInputComponent(UInputComponent* InputComp)
{
	Super::SetupPlayerInputComponent(InputComp);

	InputComp->BindAxis(TEXT("MoveForward"), this, &ATopDownShooterCharacter::InputAxisX);
	InputComp->BindAxis(TEXT("MoveRight"), this, &ATopDownShooterCharacter::InputAxisY);
}


void ATopDownShooterCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATopDownShooterCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATopDownShooterCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisX);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisY);

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController)
	{
		FHitResult ResultHit;
		MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);

		float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		SetActorRotation(FQuat(FRotator(0.f, FindRotatorResultYaw, 0.f)));


		//Sprint System
		FVector InputVector = GetCharacterMovement()->GetLastInputVector();
		FVector CursorLocation = ResultHit.Location;
		ACharacter* myCharacter = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
		FVector CharLocation = myCharacter->GetActorLocation();
		
		if (InputVector.X == 0 && InputVector.Y != 0)
		{
			float CoefK = (CursorLocation.Y - CharLocation.Y) / (CursorLocation.X - CharLocation.X);
			bool LineInRange = ((CoefK <= -sqrt(3)) || (CoefK >= sqrt(3)));
			if (InputVector.Y == 1)
			{
				CanSprint = (LineInRange && CharLocation.Y < CursorLocation.Y);
			}
			else
			{
				CanSprint = (LineInRange && CharLocation.Y > CursorLocation.Y);
			}
		}
		else
		{
			if (InputVector.X != 0 && InputVector.Y == 0)
			{
				float CoefK = (CursorLocation.X - CharLocation.X) / (CursorLocation.Y - CharLocation.Y);
				bool LineInRange = ((CoefK <= -sqrt(3)) || (CoefK >= sqrt(3)));
				if (InputVector.X == 1)
				{
					CanSprint = (LineInRange && (CursorLocation.X > CharLocation.X));
				}
				else
				{
					CanSprint = (LineInRange && (CursorLocation.X < CharLocation.X));
				}
			}
			else
			{
				if ((InputVector.X == 1 && InputVector.Y == 1) || (InputVector.X == -1 && InputVector.Y == -1))
				{
					float CoefK = (CursorLocation.X - CharLocation.X) / (CursorLocation.Y - CharLocation.Y);
					bool LineInRange = ((CoefK <= -(2 - sqrt(3))) || (CoefK >= (2 - sqrt(3))));
					if (InputVector.X == 1 && InputVector.Y == 1)
					{
						bool LineIsCorrect = (CursorLocation.X >= CharLocation.X) && (CursorLocation.Y >= CharLocation.Y);
						CanSprint = (LineInRange && LineIsCorrect);
					}
					else
					{
						bool LineIsCorrect = (CursorLocation.X <= CharLocation.X) && (CursorLocation.Y <= CharLocation.Y);
						CanSprint = (LineInRange && LineIsCorrect);
					}
				}
				else
				{
					float CoefK = (CursorLocation.Y - CharLocation.Y) / (CursorLocation.X - CharLocation.X);
					bool LineInRange = ((CoefK <= -(2 - sqrt(3))) || (CoefK >= (2 - sqrt(3))));
					if (InputVector.X == 1 && InputVector.Y == -1)
					{
						bool LineIsCorrect = (CursorLocation.X >= CharLocation.X) && (CursorLocation.Y <= CharLocation.Y);
						CanSprint = (LineInRange && LineIsCorrect);
					}
					else
					{
						bool LineIsCorrect = (CursorLocation.X <= CharLocation.X) && (CursorLocation.Y >= CharLocation.Y);
						CanSprint = (LineInRange && LineIsCorrect);
					}
				}
			}
		}

	}
}

void ATopDownShooterCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::AimState:
		ResSpeed = MovementInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalkState:
		ResSpeed = MovementInfo.AimSpeedWalk;
		break;
	case EMovementState::WalkState:
		ResSpeed = MovementInfo.WalkSpeedNormal;
		break;
	case EMovementState::RunState:
		ResSpeed = MovementInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRunState:
		ResSpeed = MovementInfo.SprintRunSpeed;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;

}


void ATopDownShooterCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::RunState;
	}
	else
	{
		if (SprintRunEnabled)
		{
		WalkEnabled = false;
		AimEnabled = false;
		MovementState = EMovementState::SprintRunState;

		}
		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalkState;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::WalkState;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::AimState;
				}
			}
		}
	}
	CharacterUpdate();

}